module gitlab.com/quocbang/barcode

go 1.20

require (
	github.com/boombuler/barcode v1.0.1
	github.com/rs/xid v1.5.0
)
