package main

import (
	"image/png"
	"log"
	"os"
	"strings"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code39"
	"github.com/rs/xid"
)

func main() {
	ID := strings.ToUpper(xid.New().String())

	// Create the barcode
	bc, err := code39.Encode(ID, false, true)
	if err != nil {
		log.Fatal(err)
	}

	// Scale the barcode to 200x200 pixels
	bc39, err := barcode.Scale(bc, 80*10, 20*10)
	if err != nil {
		log.Fatal(err)
	}

	// create the output file
	file, _ := os.Create("qrcode.png")
	defer file.Close()

	// encode the barcode as png
	png.Encode(file, bc39)
}
